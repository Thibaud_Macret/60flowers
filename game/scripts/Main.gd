extends Spatial

var is_placing:bool = false

#Tile follow
onready var camera = $CameraMech/Camera
var rayOrigin = Vector3()
var rayEnd = Vector3()

# warning-ignore:unused_argument
func _physics_process(delta):
	if(is_placing):
		var space_state = get_world().direct_space_state
		var mouse_position = get_viewport().get_mouse_position()
		
		rayOrigin = camera.project_ray_origin(mouse_position)
		rayEnd = rayOrigin + camera.project_ray_normal(mouse_position) * 2000
		var intersect = space_state.intersect_ray(rayOrigin, rayEnd, [$PlacementTile])
		
		if not intersect.empty():
			$PlacementTile.translation = (Vector3(round_tile_pos(intersect.position.x), 1, round_tile_pos(intersect.position.z)))

func round_tile_pos(pos:float)->float:
	#since coords start from the center, we need to modify the value by half-tile size
	return (sign(pos) * int((abs(pos)+6.5) /13)*13)



#Tile placement
var tileObject = preload("res://prefabs/Tile.tscn")

func _on_Button_add_pressed():
	is_placing = true
	$PlacementTile.visible = true

func _input(event):
	if(event is InputEventMouseButton and event.button_index == BUTTON_LEFT):
		if(is_placing == true):
			if(!$PlacementTile.get_colliding_bodies()): #if case is free
				place_tile()

func place_tile():
	var tileInstance = tileObject.instance()
	tileInstance.translation = $PlacementTile.translation
	tileInstance.translate(Vector3(0,-1,0))
	$Tiles.add_child(tileInstance)
	is_placing = false
	$PlacementTile.visible = false
