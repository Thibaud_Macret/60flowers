extends StaticBody

func _ready():
	randomize()
	generate_flowers()

func generate_flowers():
	var path = "res://prefabs/Flower" + String(randi()%6+1) + ".tscn"
	var flowerObject = load(path)
	var nbFlower = randi()%100+100
	for flower in range (nbFlower):
		var flowerInstance = flowerObject.instance()
		self.add_child(flowerInstance)
		#index is flower +1 because child #1 is the base
		self.get_child(flower+1).translate(Vector3((randi()%13-6), 0, (randi()%13-6)))
