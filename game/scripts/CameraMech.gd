extends Spatial

const camSpeed = 50
const camRotationSpeed = 7

func _physics_process(delta):
	#basic movement
	if(Input.is_action_pressed("ui_up")):
		self.translate(Vector3(0, 0, -delta*camSpeed))
	if(Input.is_action_pressed("ui_down")):
		self.translate(Vector3(0, 0, delta*camSpeed))
	if(Input.is_action_pressed("ui_right")):
		self.translate(Vector3(delta*camSpeed, 0, 0))
	if(Input.is_action_pressed("ui_left")):
		self.translate(Vector3(-delta*camSpeed, 0, 0))
	#Zoom
	if(Input.is_action_just_released("m4_up")):
		var scale = $"../Tiles".get_scale()
		if(scale.x < 4):
			$"../Tiles".set_scale(scale*2)
	if(Input.is_action_just_released("m4_down")):
		var scale = $"../Tiles".get_scale()
		if(scale.x > 1):
			$"../Tiles".set_scale(scale/2)
	#rotation
	if(Input.is_action_pressed("rotate_left")):
		self.rotate_y(camRotationSpeed * delta)
	if(Input.is_action_pressed("rotate_right")):
		self.rotate_y(-camRotationSpeed * delta)
